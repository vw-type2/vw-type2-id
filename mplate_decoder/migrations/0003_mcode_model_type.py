# Generated by Django 2.2.4 on 2019-08-17 09:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mplate_decoder', '0002_mcode_is_special_code'),
    ]

    operations = [
        migrations.AddField(
            model_name='mcode',
            name='model_type',
            field=models.CharField(blank=True, max_length=30),
        ),
    ]
