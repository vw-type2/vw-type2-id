# Generated by Django 2.2.11 on 2020-03-07 13:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mplate_decoder', '0015_auto_20200304_2040'),
    ]

    operations = [
        migrations.AddField(
            model_name='mplate',
            name='editor_remarks',
            field=models.TextField(blank=True),
        ),
    ]
