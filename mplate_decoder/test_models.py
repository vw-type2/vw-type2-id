from django.test import TestCase
from mplate_decoder.models import Mplate
from datetime import date


class MplateDecodeTestCase(TestCase):

    LATE_BAY_01_CHASSIS = "92023025"
    LATE_BAY_02_CHASSIS = "22138101"
    LATE_BAY_03_CHASSIS = "02057644"
    EARLY_BAY_01_CHASSIS = "9123833"

    def setUp(self):
        Mplate.objects.create(
            chassis_number_short=self.LATE_BAY_01_CHASSIS,
            m_codes_1='',
            m_codes_2='D03 P22 005 227',
            paint_and_interior_code='9451EB',
            production_date_code='382',
            production_planned='7494',
            export_destination_code='UT',
            model_code='2319',
            aggregate_code='61',
            emden='',
        )

        Mplate.objects.create(
            chassis_number_short=self.LATE_BAY_02_CHASSIS,
            m_codes_1='A89 089 100 119',
            m_codes_2='',
            paint_and_interior_code='918551',
            production_date_code='105',
            production_planned='7490',
            export_destination_code='056',
            model_code='2319',
            aggregate_code='31',
            emden='',
        )

        Mplate.objects.create(
            chassis_number_short=self.LATE_BAY_03_CHASSIS,
            m_codes_1='',
            m_codes_2='C88 072 119',
            paint_and_interior_code='474751',
            production_date_code='431',
            production_planned='',
            export_destination_code='KN',
            model_code='2310',
            aggregate_code='21',
            emden='',
        )

        Mplate.objects.create(
            chassis_number_short=self.EARLY_BAY_01_CHASSIS,
            m_codes_1='',
            m_codes_2='408 095 504 507',
            paint_and_interior_code='383851',
            production_date_code='072',
            production_planned='',
            export_destination_code='PG',
            model_code='2650',
            aggregate_code='11',
            emden='',
        )

    def test_decode_model_year(self):
        """Mplate model year is correctly decoded"""

        late_bay_01 = Mplate.objects.get(
            chassis_number_short=self.LATE_BAY_01_CHASSIS)

        late_bay_02 = Mplate.objects.get(
            chassis_number_short=self.LATE_BAY_02_CHASSIS)

        early_bay_01 = Mplate.objects.get(
            chassis_number_short=self.EARLY_BAY_01_CHASSIS)

        self.assertEqual(late_bay_01.model_year, '1979')
        self.assertEqual(late_bay_02.model_year, '1972')
        self.assertEqual(early_bay_01.model_year, '1969')

    def test_decode_production_date(self):
        """Mplate production date is correctly decoded"""

        late_bay_01 = Mplate.objects.get(
            chassis_number_short=self.LATE_BAY_01_CHASSIS)

        late_bay_02 = Mplate.objects.get(
            chassis_number_short=self.LATE_BAY_02_CHASSIS)

        late_bay_03 = Mplate.objects.get(
            chassis_number_short=self.LATE_BAY_03_CHASSIS)

        early_bay_01 = Mplate.objects.get(
            chassis_number_short=self.EARLY_BAY_01_CHASSIS)

        self.assertEqual(late_bay_01.production_date_as_time,
                         date(1978, 9, 19))
        self.assertEqual(late_bay_02.production_date_as_time,
                         date(1972, 3, 10))
        self.assertEqual(late_bay_03.production_date_as_time,
                         date(1969, 10, 20))
        self.assertEqual(early_bay_01.production_date_as_time,
                         date(1969, 2, 7))
